class AddCustomerIdToItems < ActiveRecord::Migration
  def change
  	add_column :order_items, :customer_id, :integer, default: 1, null: false
  	change_column_default(:order_items, :customer_id, nil)
  	add_index :order_items, :customer_id
  end
end
