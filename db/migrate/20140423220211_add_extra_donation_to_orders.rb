class AddExtraDonationToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :extra_donation, :decimal, default: 0
  end
end
