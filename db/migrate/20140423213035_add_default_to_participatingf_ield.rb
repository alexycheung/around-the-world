class AddDefaultToParticipatingfIeld < ActiveRecord::Migration
  def change
  	change_column :vendors, :participating, :boolean, default: false
  end
end
