class IndexCustomersAndVendorsByName < ActiveRecord::Migration
  def change
  	add_index :customers, :name
  	add_index :vendors, :name
  end
end
