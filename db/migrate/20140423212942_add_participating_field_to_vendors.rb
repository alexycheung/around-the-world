class AddParticipatingFieldToVendors < ActiveRecord::Migration
  def change
  	add_column :vendors, :participating, :boolean
  end
end
