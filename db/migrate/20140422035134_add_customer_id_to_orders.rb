class AddCustomerIdToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :customer_id, :integer
  	add_column :orders, :user_id, :integer
  	add_index :orders, [:customer_id, :user_id]
  end
end
