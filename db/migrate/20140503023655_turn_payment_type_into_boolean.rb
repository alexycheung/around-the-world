class TurnPaymentTypeIntoBoolean < ActiveRecord::Migration
  def change
  	add_column :orders, :cash, :boolean, default: true
  end
end
