class AddDefaultToOrders < ActiveRecord::Migration
  def change
  	change_column :orders, :total, :decimal, default: 0
  	change_column :orders, :donated, :decimal, default: 0
  end
end
