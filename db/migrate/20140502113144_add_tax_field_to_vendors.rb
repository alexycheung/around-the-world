class AddTaxFieldToVendors < ActiveRecord::Migration
  def change
  	add_column :vendors, :tax, :boolean, default: false
  end
end
