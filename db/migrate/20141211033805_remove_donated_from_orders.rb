class RemoveDonatedFromOrders < ActiveRecord::Migration
  def change
  	remove_column :orders, :donated, :decimal
  	remove_column :orders, :user_id, :integer
  	remove_column :orders, :extra_donation, :decimal
  	remove_column :orders, :cash, :boolean
  end
end
