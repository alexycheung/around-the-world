class RemoveParticipatingFromVendors < ActiveRecord::Migration
  def change
  	remove_column :vendors, :participating, :boolean
  end
end
