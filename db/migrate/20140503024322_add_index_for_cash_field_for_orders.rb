class AddIndexForCashFieldForOrders < ActiveRecord::Migration
  def change
  	add_index :orders, :cash
  end
end
