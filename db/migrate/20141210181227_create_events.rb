class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
    	t.string :name, null: false
      t.timestamps
    end

    add_column :orders, :event_id, :integer
    add_column :vendors, :event_id, :integer
    add_index :orders, :event_id
    add_index :vendors, :event_id
  end
end
