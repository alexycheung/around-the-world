class RemoveCustomerIdFromOrderItems < ActiveRecord::Migration
  def change
  	remove_column :order_items, :customer_id
  end
end
