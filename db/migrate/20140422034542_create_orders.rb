class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
    	t.decimal :total
    	t.string :payment_type
    	t.decimal :donated
    end
  end
end
