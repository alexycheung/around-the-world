class AddVendorIdToOrderItems < ActiveRecord::Migration
  def change
  	add_column :order_items, :vendor_id, :integer
  end
end
