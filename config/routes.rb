AroundTheWorld::Application.routes.draw do
  get "events/index"
  #Static Pages
  root 'events#index'

  resources :events
  match '/events/:id/orders', to: 'events#event_orders', via: 'get', as: 'event_orders'
  match '/events/:id/vendors', to: 'events#event_vendors', via: 'get', as: 'event_vendors'
  match '/events/:id/analytics', to: 'events#event_analytics', via: 'get', as: 'event_analytics'

  resources :users
  match '/signup', to: 'users#new', via: 'get'

  resources :sessions, only: [:new, :create, :destroy]
  match '/signin', to: 'sessions#new', via: 'get'
  match '/signout', to: 'sessions#destroy', via: 'delete'

  resources :vendors
  match '/vendors/:id/history', to: 'vendors#history', via: 'get'

  resources :customers do
    member do
      get 'place_order'
    end
  end

  resources :orders do
    member do
      get 'open'
      get 'backtrack'
    end
  end

  resources :order_items

  resources :analytics

  match '/home', to: 'static_pages#home', via: 'get'
end