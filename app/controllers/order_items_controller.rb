class OrderItemsController < ApplicationController
	respond_to :html, :json

	def create
		@order_item = OrderItem.new(order_item_params)
		@saved = @order_item.save

		respond_to do |format|
			format.js
		end
	end

	def update
		@order_item = OrderItem.find(params[:id])
		@updated = @order_item.update_attributes(order_item_params)
		@customer = @order_item.customer

		if @updated && @order_item.order_id
			@order = @order_item.order
			@order.recalculate_total_price
		end

		respond_to do |format|
			format.js
		end
	end

	def destroy
		@order_item = OrderItem.find(params[:id])
		@destroyed = @order_item.destroy
		@customer = @order_item.customer

		if @destroyed && @order_item.order_id
			@order = @order_item.order
			@order.recalculate_total_price
		end

		respond_to do |format|
			format.js
		end
	end

	private

		def order_item_params
			params.require(:order_item).permit(:order_id, :vendor_id, :price, :quantity, :customer_id)
		end
end