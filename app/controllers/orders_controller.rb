class OrdersController < ApplicationController
	def backtrack
		@order = Order.find(params[:id])
		@customer = @order.customer
		@event = @order.event
		@order_items = @order.order_items
		@order_items.update_all(order_id:nil)
		@destroyed = @order.destroy

		respond_to do |format|
			format.js
		end
	end

	def open
		@order = Order.find(params[:id])
		@event = @order.event
		@customer = @order.customer
		@order_items = @order.order_items.recently
	end

	def index
		@orders = Order.recent_ones_first.paginate(page: params[:page], per_page: 10)
	end

	def show
		@order = Order.find(params[:id])
		@order_items = @order.order_items
	end

	def create
		@order = Order.new(order_params)
		@order.total = @order.customer.calculate_total
		@saved = @order.save

		@customer = @order.customer

		#update order id of all orderless order items
		@customer.order_items.orderless.complete.update_all(order_id:@order.id)

		respond_to do |format|
			format.js
		end
	end

	def edit
		@order = Order.find(params[:id])
	end

	def update
		@order = Order.find(params[:id])
		@updated = @order.update_attributes(order_params)
		
		respond_to do |format|
			format.js
		end
	end

	def destroy
		@order = Order.find(params[:id])
		@destroyed = @order.destroy

		respond_to do |format|
			format.js
		end
	end

	private

		def order_params
			params.require(:order).permit(:total, :payment_type, :customer_id, :event_id)
		end
end
