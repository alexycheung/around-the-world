class VendorsController < ApplicationController
	respond_to :html, :json

	def index
		@vendors = Vendor.recent_ones_first
	end

	def history
		@vendor = Vendor.find(params[:id])
		@order_items = OrderItem.where(vendor_id: @vendor)
	end

	def create
		@vendor = Vendor.new(vendor_params)
		@saved = @vendor.save

		respond_to do |format|
			format.js
		end
	end

	def edit
		@vendor = Vendor.find(params[:id])
	end

	def update
		@vendor = Vendor.find(params[:id])
		@updated = @vendor.update_attributes(vendor_params)
		respond_to do |format|
			format.js
		end
	end

	def destroy
		@vendor = Vendor.find(params[:id])
		@destroyed = @vendor.destroy

		respond_to do |format|
			format.js
		end
	end

	private

		def vendor_params
			params.require(:vendor).permit(:tax, :name, :email, :participating, :event_id, :user_id)
		end
end