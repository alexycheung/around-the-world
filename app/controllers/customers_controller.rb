class CustomersController < ApplicationController
	respond_to :html, :json

	def place_order
		@customer = Customer.find(params[:id])
		@order_items = @customer.order_items.recently
		@event = Event.find_by_id(params[:event_id])
		respond_to do |format|
			format.js
		end
	end

	def index
		@customers = Customer.recent_ones_first

		if signed_in?
			if params[:query].present?
	      @customers = @customers.search(params[:query], operator: "or", fields: [:name])
	    else
	      @customers = @customers.paginate(page: params[:page], per_page: 20)
			end
		else
			redirect_to "/home"
			flash[:notice]="Sign in below!"
		end
	end

	def create
		@customer = Customer.new(customer_params)
		@customer.user_id = current_user.id
		@saved = @customer.save
		@event = Event.find_by_id(params[:event_id])
		@order_items = @customer.order_items.recently

		respond_to do |format|
			format.js
		end
	end

	def edit
		@customer = Customer.find(params[:id])
	end

	def update
		@customer = Customer.find(params[:id])
		@updated = @customer.update_attributes(customer_params)
		respond_to do |format|
			format.js
		end
	end

	def destroy
		@customer = Customer.find(params[:id])
		@destroyed = @customer.destroy
		respond_to do |format|
			format.js
		end
	end

	private

		def customer_params
			params.require(:customer).permit(:name, :email, :user_id)
		end
end
