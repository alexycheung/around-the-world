class AnalyticsController < ApplicationController
	def index
		@orders = Order.all
		@customers = Customer.all
		@order_items = OrderItem.all
		@vendors = Vendor.all
	end
end
