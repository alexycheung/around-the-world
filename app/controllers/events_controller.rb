class EventsController < ApplicationController
	def event_analytics
		@event = Event.find(params[:id])
		@orders = @event.orders.recently
		@total_revenue = @orders.sum(:total)
		@customer_count = @orders.collect(&:customer).uniq.count
		@average_spending = @total_revenue/@customer_count.to_d
		@cash_total = @orders.where(payment_type:'cash').sum(:total)
		@check_total = @orders.where(payment_type:'check').sum(:total)
		@card_total = @orders.where(payment_type:'card').sum(:total)
		@vendors = @event.vendors.recently
	end

	def event_orders
		@event = Event.find(params[:id])
		@orders = @event.orders.recently.paginate(page: params[:page], per_page: 20)
	end

	def event_vendors
		@event = Event.find(params[:id])
		if params[:query].present?
			@search = true
      @vendors = Vendor.all.search(params[:query], where: {event_id: @event.id}, fields: [{name: :word_start}])
    else
      @vendors = @event.vendors.recently
		end
	end

	def show
		@event = Event.find(params[:id])
		if params[:query].present?
			@search = true
      @customers = Customer.search(params[:query], where: {user_id: current_user.id}, fields: [{name: :word_start}])
    else
      @customers = current_user.customers.recently.paginate(page: params[:page], per_page: 20)
		end
	end

	def create
		@event = Event.new(event_params)
		@event.user_id = current_user.id
		@user = @event.user
		@customers = @user.customers.recently
		@saved = @event.save

		if @saved
			redirect_to event_path(@event)
		end
	end

  def index
  	if signed_in?
  		if current_user.events.any?
  			redirect_to event_path(current_user.events.recently.first)
  		end
  	else
  		redirect_to signin_path
  	end
  end

  private
  	def event_params
  		params.require(:event).permit(:name, :user_id)
  	end
end
