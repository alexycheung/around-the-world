module OrdersHelper
	#show if cash or check was selectred
	def cash_selected(order)
		"selected" if order.payment_type == "cash"
	end

	def check_selected(order)
		"selected" if order.payment_type=="check"
	end

	def card_selected(order)
		"selected" if order.payment_type=="card"
	end
end
