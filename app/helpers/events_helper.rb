module EventsHelper
	#select correct link on side navbar
	def selected(event)
		'selected' if current_page?(event_path(event)) || current_page?(event_orders_path(event)) || current_page?(event_analytics_path(event)) || current_page?(event_vendors_path(event))
	end

	#select correct link on event dashboard
	def selected_event_link(path)
		'selected' if current_page?(path)
	end
end
