module VendorsHelper
	#highlight vendor in analytics if total sold > 0
	def selected_row(vendor)
		'selected-row' if vendor.total_sold!=0.0
	end
end