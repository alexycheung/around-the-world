class Customer < ActiveRecord::Base
	searchkick word_start: [:name]

	#relationships
	has_many :orders, dependent: :destroy
	has_many :order_items, dependent: :destroy
	belongs_to :user

	#validations
	validates :name, presence: true

	#scopes
	scope :recently, -> {order("created_at DESC")}

	def calculate_total
		total = 0.0

		self.order_items.orderless.complete.each do |order_item|
			if order_item.vendor.tax
				total = total + 1.0875 * order_item.price * order_item.quantity
			else
				total = total+order_item.price*order_item.quantity
			end
		end

		return total
	end
end