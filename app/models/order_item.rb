class OrderItem < ActiveRecord::Base
	#relationships
	belongs_to :order
	belongs_to :vendor
	belongs_to :customer

	#validations
	validates :vendor_id, presence: true, numericality: true

	#scopes
	scope :recently, -> {order("created_at DESC")}
	scope :orderless, -> {where("order_id IS NULL")}
	scope :complete, -> {where("quantity IS NOT NULL").where("price IS NOT NULL")}
end