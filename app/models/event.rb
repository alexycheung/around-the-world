class Event < ActiveRecord::Base
	searchkick
	
	#validations
	validates :name, presence: true

	#relationships
	has_many :orders
	has_many :vendors
	belongs_to :user

	#scopes
	scope :recently, -> {order("created_at DESC")}
end
