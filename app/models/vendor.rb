class Vendor < ActiveRecord::Base
	searchkick word_start: [:name]
	
	#validations
	validates :name, presence: true
	
	#relationships
	has_many :order_items
	belongs_to :event

	#scopes
	scope :alphabetical, -> {order('name ASC')}
	scope :recently, -> {order('created_at DESC')}

	def total_sold
		total = 0.0

		self.order_items.complete.each do |order_item|
			if order_item.order_id
				if self.tax
					total = total + 1.0875 * order_item.price * order_item.quantity
				else
					total = total+order_item.price*order_item.quantity
				end
			end
		end

		return total
	end
end