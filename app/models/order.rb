class Order < ActiveRecord::Base

	#validations
	validates :customer_id, presence: true
	validates :event_id, presence: true

	#relationships
	has_many :order_items, dependent: :destroy
	belongs_to :customer
	belongs_to :user
	belongs_to :event

	#scopes
	scope :recently, -> {order('created_at DESC')}


  def recalculate_total_price
  	total = 0.0

		self.order_items.complete.each do |order_item|
			if order_item.vendor.tax
				total = total + 1.0875 * order_item.price * order_item.quantity
			else
				total = total+order_item.price*order_item.quantity
			end
		end

		self.total = total
		save!
	end
end