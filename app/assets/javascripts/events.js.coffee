ready = ->
	#load form to add new customer
	$("body").on "keyup", "#customer-search-field", (e) ->
	 	if e.keyCode is 13
	 		$("#customer-overlay").show()
			$("#customer_name").val($("#customer-search-field").val())
			$("#customer_email").focus()
	  return

	#load form to add new vendor
	$("body").on "keyup", "#vendor-search-field", (e) ->
	 	if e.keyCode is 13
	 		$("#vendor-overlay").show()
			$("#vendor_name").val($("#vendor-search-field").val())
			$("#vendor_email").focus()
	  return

	#search after pause
  $("body").on "keyup", "#customer-search-field", (e) ->
    value = $("#customer-search-field").val()
    setTimeout (->
      $(".search-customers-form").submit() if $("#customer-search-field").val() is value
      return
    ), 75
    return

  #search after pause
  $("body").on "keyup", "#vendor-search-field", (e) ->
    value = $("#vendor-search-field").val()
    setTimeout (->
      $(".search-vendors-form").submit() if $("#vendor-search-field").val() is value
      return
    ), 75
    return

#make coffeescript work with turbolinks redirect
$(document).ready(ready)
$(document).on('page:load', ready)