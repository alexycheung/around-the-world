ready = ->
	#select cash or check
	$("body").on "click", ".select-cash", (e) ->
		$("#order_payment_type").val "cash"
		$('#order_payment_type .selected').removeClass('selected') if $('#order_payment_type .selected')[0]
		$(".select-cash").addClass('selected')
		$("#payment-type-modal input[type='submit']").click()
		return

	$("body").on "click", ".select-check", (e) ->
		$("#order_payment_type").val "check"
		$('#order_payment_type .selected').removeClass('selected') if $('#order_payment_type .selected')[0]
		$(".select-check").addClass('selected')
		$("#payment-type-modal input[type='submit']").click()
		return

	$("body").on "click", ".select-card", (e) ->
		$("#order_payment_type").val "card"
		$('#order_payment_type .selected').removeClass('selected') if $('#order_payment_type .selected')[0]
		$(".select-card").addClass('selected')
		$("#payment-type-modal input[type='submit']").click()
		return

#make coffeescript work with turbolinks redirect
$(document).ready(ready)
$(document).on('page:load', ready)