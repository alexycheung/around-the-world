ready = ->
	#exit form
	$('html').on 'click', '.exit-form', (e) ->
		$('.modal-overlay:not(#customer-overlay, #vendor-overlay, #event-overlay)').remove()
		return

	$('html').on 'click', '.exit-customer-form', (e) ->
		$('#new_customer')[0].reset()
		$('#customer-overlay').hide()
		$('#customer-search-field').select()
		return

	$('html').on 'click', '.exit-vendor-form', (e) ->
		$('#new_vendor')[0].reset()
		$('#vendor-overlay').hide()
		$('#vendor-search-field').select()
		return

	$('html').on 'click', '.exit-event-form', (e) ->
		$('#new_event')[0].reset()
		$('#event-overlay').hide()
		return


	#load form to add new event
	$('.add-event-button').click ->
		$('#event-overlay').show()
		$('#event_name').focus()
		return

#make coffeescript work with turbolinks redirect
$(document).ready(ready)
$(document).on('page:load', ready)