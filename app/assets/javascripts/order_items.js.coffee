ready = ->
	#select order item fields on focus
	$("body").on "focus", "#order_item_price", (e) ->
		$(this).select()
		return

	$("body").on "focus", "#order_item_quantity", (e) ->
		$(this).select()
		return

	#submit order item form on any change
	
	$("body").on "keyup", "#order_item_price", (e) ->
		$(this).parent().parent("form").submit()
		return

	$("body").on "keyup", "#order_item_quantity", (e) ->
		$(this).parent().parent("form").submit()
		return

#make coffeescript work with turbolinks redirect
$(document).ready(ready)
$(document).on('page:load', ready)